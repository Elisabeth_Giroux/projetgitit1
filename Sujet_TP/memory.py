import numpy as np
import random

"""
@author vcoindet
Jeu de memory
"""

Tabl = ['a', 'a', 'b', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'f', 'f']

def melange_carte(Tab):
    """
        Fonction qui permet de mélanger les éléments d'un tableau donnée en argument.
        :param Tab: list
        :return value: list
    """
    for i in range(1, len(Tab)):
        # pick an element in Tab[:i+1] with which to exchange Tab[i]
        j = int(random.random() * (i+1))
        k = int(random.random() * (i+1))
        Tab[i], Tab[j] = Tab[k], Tab[i]
    return Tab

def carte_cache(Tab):   #TODO
    """
        Doit créer une liste de même longueur que la liste renvoyés dans la question précédentes
        :param Tab: list
        :return value: list
    """
    L_carte_cache = [0 for _ in range(len(Tab))]
    return L_carte_cache


def choisir_cartes(Tab):
    """
        Demande à l'utilisateur de choisir deux cartes différentes et les renvoie
        sous forme de liste.
        :param Tab: list
        :return value: list
    """
    c1 = int(input("Choisissez une carte : "))-1
    if c1 < 0 or c1 > len(Tab)-1:
        print("Erreur, la carte n'existe pas!")
        c1 = int(input("Veuillez choisir une autre carte: "))
    print(Tab[c1])
    c2 = int(input("Choisissez une deuxieme carte : "))-1
    if c2 < 0 or c2 > len(Tab)-1:
        print("Erreur, la carte n'existe pas!")
        c2 = int(input("Veuillez choisir une autre carte: "))
    while c1 == c2:
        print("Erreur, la deuxieme carte ne peut être la même que la premiere ! ")
        c2 = int(input("Veuillez choisir une deuxieme carte différente de la premiere : "))
    print(Tab[c2])
    return [c1,c2]


def retourne_carte (c1, c2, Tab, Tab_cache):    #TODO
    """
        Doit retrouner les cartes dans la liste cachée
        :param c1: str
        :param c2: str
        :param Tab: list
        :param Tab_cache: list
    """
    Tab_cache[c1] = Tab[c1]
    Tab_cache[c2] = Tab[c2]
    return Tab_cache

def jouer(Tab):
    """
        Fonction principale qui contient la logique interne du jeu et permet
        de jouer au memory.
        :param Tab: list
        
    """
    print("Bienvenue sur notre Jeu Memory, ci dessous voici les cartes mise en jeu. Il y en a 12")
    print(Tab)
    Tab = melange_carte(Tab)
    print("Maintenant je vais melanger les cartes et les retourner")
    
    Tab_cache =  carte_cache(Tab)
    print(Tab_cache)

    [c1, c2] = choisir_cartes(Tab)
    Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
    print(Tab_cache)
    
    while 0 in Tab_cache:
        [c1, c2] = choisir_cartes(Tab)
        
        Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
        
        print(Tab_cache)
        
    print("Bravo tu as gagné!!!")

    
jouer(Tabl)


if __name__=="__main__":
    Tabl = [1,1,2,2,3,3,4,4,5,5,6,6]

    
    #test unitaire choisir_cartes
    Tab=melange_carte(Tabl)
    print(choisir_cartes(Tab))
    
    #test unitaire retourne_carte 
    Tab_cache=carte_cache(Tab)
    c1=choisir_cartes(Tab)[0]
    c2=choisir_cartes(Tab)[1]
    print(retourne_carte(c1,c2,Tab, Tab_cache))
      

    #Test de melange_carte
    print(melange_carte(Tabl))
    
    #Test de carte_cache
    print(carte_cache(Tabl))


